/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#ifndef	COMMON_H
#define COMMON_H

/* Includes */
#include "../definitions.h"
#include "../modules/microcontroller.h"
#include "../modules/battery_charger.h"

/* Function declarations */
void InitRegister(void);
void InitModules(void);
void RegWrite(const uint8_t targetReg, const uint8_t value);
void TriggerMeasurements(void);

#endif
