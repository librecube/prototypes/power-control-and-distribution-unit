/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#ifndef	MICROCONTROLLER_H
#define MICROCONTROLLER_H

/* Includes */
#include "../definitions.h"
#include "c8051f310.h"

/* ADC specific */
#define ADC_INTERNAL            1
#define ADC_EXTERNAL            2
#define ADC_CUR_PORT            0
#define ADC_VOL_PORT            12

/* Pin allocations */
#define BCM_EN                  P0_5
#define BCM_STATUS_1            P0_7
#define BCM_STATUS_2            P0_6

/* Function declarations */
void MicrocontrollerInit(void);
void MicrocontrollerReset(void);
void AdcState(const uint8_t state); 
uint16_t AdcMeasure(const uint8_t type, const uint8_t port);

#endif
